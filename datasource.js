'use strict';
// Database
var mongoose = require('mongoose');
var Product = require("./models/product").Product;
var Price = require("./models/price").Price;
var Place = require("./models/place").Place;

// Database connection
mongoose.connect('mongodb://localhost/products');

// Lugares
exports.getPlaces = function(res) {
  Place.find(function(err, places) {
    if (err) console.log('error occured in the database');
    res.send({ result: places });
  });
};

exports.createPlace = function(data, res) {
  var newPlace = new Place(data);

  newPlace.save(function (err, result) {
    if (err) return console.error(err);
    res.send({ result: true });
  });
};

exports.deletePlace = function(placeId, res) {
  Place.remove({ _id: placeId }, function(err) {
      if (err) return console.error(err);
      res.send({ result: true });
  });
};

// Productos
exports.getProducts = function(res) {
  Product
    .find()
    .populate('prices')
    .exec(function(err, products) {
      if (err) console.log('error occured in the database');
      res.send({ result: products });
    });
};

exports.getProductByUPC = function(upc, res) {
  Product
    .find({ upc: upc })
    .populate('prices')
    .exec(function(err, product) {
      if (err) console.log('error occured in the database');
      res.send({ result: product });
    });
};

exports.createProduct = function(data, res) {
  var newProduct = new Product(data);

  newProduct.save(function (err, result) {
    if (err) return console.error(err);
    res.send({ result: true });
  });
};

exports.deleteProduct = function(productId, res) {
  Product.remove({ _id: productId }, function(err) {
      if (err) return console.error(err);
      res.send({ result: true });
  });
};

// Precios
exports.getPrices = function(res) {
  Price.find(function(err, prices) {
    if (err) console.log('error occured in the database');
    res.send({ result: prices });
  });
};

exports.getPriceByUPC = function(upc, res) {
  Price.find({ upc: upc }, function(err, prices) {
    if (err) console.log('error occured in the database');
    res.send({ result: prices });
  });
};

exports.createPrice = function(data, res) {
  var newPrice = new Price(data);

  newPrice.save(function (err, result) {
    if (err) return console.error(err);

    Product
      .findOne({ upc: result.upc })
      .exec(function(err, prod) {
        console.log(prod);
        prod.prices.push(result._id);

        prod.save(function(err, response) {
          if (err) console.log('error occured in the database');
          console.log(response);
          res.send({ result: true });
        });

      });
  });
};

exports.deletePrice = function(priceId, res) {
  Price.remove({ _id: priceId }, function(err) {
      if (err) return console.error(err);
      res.send({ result: true });
  });
};
