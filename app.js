// Application
var bodyParser = require('body-parser');
var express = require('express');
var db = require('./datasource');
var app = express();

app.use(bodyParser.urlencoded({ extended: false }));

// Lugares
app.get('/lugares', function (req, res) {
    db.getPlaces(res);
});

app.post('/lugar', function (req, res) {
    db.createPlace({ name: req.body.name }, res);
});

app.delete('/lugar/:id', function (req, res) {
    db.deletePlace(req.params.id, res);
});

// Productos
app.get('/productos', function (req, res) {
    db.getProducts(res);
});

app.get('/producto/:upc', function (req, res) {
    db.getProductByUPC(req.params.upc, res);
});

app.post('/producto', function (req, res) {
    db.createProduct({
      upc: req.body.upc,
      description: req.body.description,
      prices: []
    }, res);
});

app.delete('/producto/:id', function (req, res) {
    db.deleteProduct(req.params.id, res);
});

// Precios
app.get('/precios', function (req, res) {
    db.getPrices(res);
});

app.get('/precio/:upc', function (req, res) {
    db.getPriceByUPC(req.params.upc, res);
});


app.post('/precio', function (req, res) {
    var data = {
      upc: req.body.upc,
      placeId: req.body.placeId,
      // Format -> 02/03/2016
      date: req.body.date,
      price: req.body.price
    };
    db.createPrice(data, res);
});

app.delete('/precio/:id', function (req, res) {
    db.deletePrice(req.params.id, res);
});


app.listen(8080, function () {
  console.log('App listening on port 8080!');
});

exports = module.exports = app;
