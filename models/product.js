var mongoose = require("mongoose");

var ProductSchema = new mongoose.Schema({
  upc: { type: String, index: true },
  description: { type: String },
  prices: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Price' }]
});

var Product = mongoose.model('Product', ProductSchema);

module.exports = {
  Product: Product
};
