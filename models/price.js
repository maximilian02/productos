var mongoose = require("mongoose");

var PriceSchema = new mongoose.Schema({
  upc: { type: String},
  placeId: { type: String },
  date: { type: String },
  price: { type: String}
});

var Price = mongoose.model('Price', PriceSchema);

module.exports = {
  Price: Price
};
